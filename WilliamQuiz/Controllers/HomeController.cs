﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WilliamQuiz.Models.QuizModels;

namespace WilliamQuiz.Controllers

    // hej 
{
    public class HomeController : Controller
    {
          
        public ActionResult Index()
        {
            Evaluation evaluation = GetEvaluation();

            return View(evaluation);
        }

        private static Evaluation GetEvaluation()
        {
            var evaluation = new Evaluation();


            Question q1 = new Question(1, "Vad heter jag?");
            q1.Answers.Add(new Answer(11, "William", true));
            q1.Answers.Add(new Answer(12, "Petter-Niklas"));
            evaluation.Questions.Add(q1);

            Question q2 = new Question(2, "Vilken sport är bäst?");
            q2.Answers.Add(new Answer(21, "Hockey"));
            q2.Answers.Add(new Answer(22, "Fotboll"));
            q2.Answers.Add(new Answer(23, "Lol", true));
            evaluation.Questions.Add(q2);
            return evaluation;
        }

        [HttpPost]
        public ActionResult Index(Evaluation model)
        {
            if (ModelState.IsValid)
            {
                var originalEvaluation = GetEvaluation();
                foreach (var q in model.Questions)
                {
                    var originalQuestion = originalEvaluation.Questions.Where(oq => oq.ID == q.ID).FirstOrDefault();
                    var correctAnswer = originalQuestion.Answers.Where(a => a.IsCorrect).FirstOrDefault();

                    originalQuestion.WasCorrect = q.SelectedAnswer == correctAnswer.ID;


                    //Save

                }
                return View("Facit", originalEvaluation);
            }
            //reload questions
            return View(GetEvaluation());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}