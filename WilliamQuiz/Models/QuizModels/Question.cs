﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WilliamQuiz.Models.QuizModels
{
    public class Question
    {
        public string QuestionString { get; set; }
        public List<Answer> Answers { get; set; }
        public int ID { set; get; }
        public int SelectedAnswer { set; get; }
        public bool WasCorrect { get; set; }

        public Question() {
            Answers = new List<Answer>();
        }

        public Question(string questionString)
        {
            QuestionString = questionString;
            Answers = new List<Answer>();

        }

        public Question(int id, string questionString)
        {
            Answers = new List<Answer>();
            this.ID = id;
            this.QuestionString = questionString;
        }
    }
}

