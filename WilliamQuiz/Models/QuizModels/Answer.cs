﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WilliamQuiz.Models.QuizModels
{
    /// <summary>
    /// This is a class used to declare a quiz answer
    /// </summary>
    public class Answer
    {
        /// <summary>
        /// This is the string representation of the answer
        /// </summary>
        public string AnswerString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ID { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsCorrect { set; get; }

        /// <summary>
        /// Answer class constructor
        /// </summary>
        /// <param name="answerString">This is the string representation of the answer</param>
        /// <param name="isCorrect">Boolean parameter to define if the answer is correct or not</param>
        public Answer(int id, string answerString, bool isCorrect = false)
        {
            this.ID = id;
            this.AnswerString = answerString;
            this.IsCorrect = isCorrect;
        }
    }
}