﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WilliamQuiz.Models.QuizModels
{
    public class Evaluation
    {
        public List<Question> Questions { set; get; }

        public Evaluation()
        {
            Questions = new List<Question>();
        }
    }
}